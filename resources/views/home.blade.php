@extends('layouts.main')

@section('css')
<style>
.mainsection{
    color:white;
    margin-bottom:50px;
}
.heading{
    border-bottom:1px solid #0aff00;
    font-weight:100;
}
.heading h1,h5{
    font-weight:100;
}
.table1{
    width:100%;
    text-align: center;
    border-top:0.6px solid white;
    overflow-x:scroll;

}
table th{
    font-weight:600;
    padding:10px 0px;
    color:white;
}
table tr td{
    font-weight:100;
    height:50px;
    width:150px;
    padding:0px 13px;
    background-color: #fff;
    color:black;
}
table tr{
    margin-bottom:5px;
}
table td .edit{
   color:grey;
   
}
table td i{
font-size:25px;   
}
table td img{
    width:40px;
    height:40px;
    border-radius:50%;
    
}
.chat {
    background-color: #fff;
    color:black;
    height:250px;
}
.span2{
    color:white;
    border-radius:5px;
}
.facilitimanagement{
    border-bottom:1px solid #fd3939;
    font-weight:100;
}
   
}
.facilitimanagement h1,h5{
    font-weight:100;
}
.roomtype{
    background-color: #fff;
    color:black;
}
.roomtype .qty{
    border-left:1px solid grey;
}
.shadow {
  -moz-box-shadow:    3px 3px 5px 6px #ccc;
  -webkit-box-shadow: 3px 3px 5px 6px #ccc;
  box-shadow:         3px 3px 5px 6px #ccc;
}
.container{
    margin-top:100px;
}

  </style>
@endsection

@section('main-section')
 <!-- main section start  -->

  <!-- room booking table  -->
  <div class="container bg-dark mainsection mb-5">
     <div class="row heading  mt-5"><h1>Room Bookings</h1></div>
     <div class="row pb-3"><h5>Most recent bookings</h5></div>
     <div class="row bookingdata mb-5" style='overflow-x:auto' >
         <table class='table1' >
             
             <tr>
                 <th>Photo</th>
                 <th>Name</th>
                 <th>Mobile</th>
                 <th>Email</th>
                 <th>Arrive</th>
                 <th>Depart</th>
                 <th>Room</th>
                 <th>Payment</th>
                 <th>Admin</th>
             </tr>
             <tr class='mb-md-2 mb-0' >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>077000000</td>
                 <td>admin@gmail.com</td>
                 <td>11 sep 2020</td>
                 <td>15 sep 2020</td>
                 <td>Premium 32</td>
                 <td><span class='bg-primary p-1' style='color:white;border-radius:5px'>Approved</span></td>
                 <td><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>077000000</td>
                 <td>admin@gmail.com</td>
                 <td>11 sep 2020</td>
                 <td>15 sep 2020</td>
                 <td>Premium 32</td>
                 <td><span class='bg-danger p-1' style='color:white;border-radius:5px'>Cancelled</span></td>
                 <td><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>077000000</td>
                 <td>admin@gmail.com</td>
                 <td>11 sep 2020</td>
                 <td>15 sep 2020</td>
                 <td>Premium 32</td>
                 <td><span class='bg-primary p-1' style='color:white;border-radius:5px'>Approved</span></td>
                 <td><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>077000000</td>
                 <td>admin@gmail.com</td>
                 <td>11 sep 2020</td>
                 <td>15 sep 2020</td>
                 <td>Premium 32</td>
                 <td><span class='bg-primary p-1' style='color:white;border-radius:5px'>Approved</span></td>
                 <td><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>077000000</td>
                 <td>admin@gmail.com</td>
                 <td>11 sep 2020</td>
                 <td>15 sep 2020</td>
                 <td>Premium 32</td>
                 <td><span class='bg-warning p-1 px-2' style='color:white;border-radius:5px'>Pending</span></td>
                 <td><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
         </table>
     </div>
 </div>
  <!-- room booking table end  -->

  <!-- chat  start  -->
  <div class="container bg-dark mainsection" >
     <div class="row heading  mt-5"><h1>Chats</h1></div>
     <div class="row pb-3"><h5>Live chat with booked in students</h5></div>
     <div class="row ">
         <div class="col-md-5 col-12 p-2">
             <div class="row chat m-1">User list</div>
         </div>
         <div class="col-md-7 col-12 p-2">
             <div class="row chat m-1">Admin</div>
         </div>
     </div>

</div>


  <!-- chat  start end -->

   <!-- faciliti management start  -->
   <div class="container bg-dark mainsection mb-5" >
     <div class="row  facilitimanagement  mt-5"><h1>Facilities Management</h1></div>
     <div class="row pb-3"><h5>Building management</h5></div>
     <div class="row bookingdata mb-5" style="overflow-x:auto" >
         <table class='table2'>
             
             <tr>
                 <th>Product</th>
                 <th>Name</th>
                 <th>SKU</th>
                 <th>LOC</th>
                 <th>Reported</th>
                 <th>Issue</th>
                 <th>Status</th>
                 <th>Admin</th>
             </tr>
            
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>BOL002</td>
                 <td>Bolier Room</td>
                 <td>11 sep 2020</td>
                 <td>Plug not working</td>
                 <td><span class='bg-danger p-1 px-3 span2' >Awating part</span></td>
                 <td><i class=" m-md-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>BOL002</td>
                 <td>Bolier Room</td>
                 <td>11 sep 2020</td>
                 <td>Plug not working</td>
                 <td><span class='bg-primary p-1 px-4 span2' >Completed</span></td>
                 <td><i class=" m-md-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
            
             <tr  >
                 <td><img src="images/header-image.jpg" width='50px' height='50px' alt=""></td>
                 <td>Admin</td>
                 <td>BOL002</td>
                 <td>Bolier Room</td>
                 <td>11 sep 2020</td>
                 <td>Plug not working</td>
                 <td><span class='bg-warning p-1 px-2 span2' >Engineer called</span></td>
                 <td><i class=" m-md-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></td>
             </tr>
         </table>
     </div>
 </div>

   <!-- faciliti management start end  -->

   <!-- room profile  -->
   <div class="container bg-dark mainsection mb-5" >
     <div class="row  facilitimanagement  mt-5"><h1>Room Profiles</h1></div>
     <div class="row pb-3"><h5>Manage room profiles</h5></div>
     <div class="row  mb-5 roomtype px-4" >
         <div class="col ">
            <div class="row">
                <div class="col-lg-11 col-md-3 col-9 ">Room Types</div>
                <div class="col-lg-1 col-md-3 col-3 ">Quantities</div>
            </div>
            <div class="row py-3 my-3 shadow">
                <div class="col-lg-11 col-md-10 col-10">
                    <h3>Standard/Delux Studio</h3>
                </div>
                <div class="col-lg-1 col-md-2 col-2 qty"><h3>36</h3></div>
            </div>
            <div class="row py-3 my-3 shadow">
                <div class="col-lg-11 col-md-10 col-10">
                    <h3>Restricted view (Mezz) en-suits</h3>
                </div>
                <div class="col-lg-1 col-md-2 col-2 qty"><h3>105</h3></div>
            </div>
            <div class="row py-3 my-3 shadow">
                <div class="col-lg-11 col-md-10 col-10">
                    <h3>Internal en-suites</h3>
                </div>
                <div class="col-lg-1 col-md-2 col-2 qty"><h3>36</h3></div>
            </div>
            <div class="row py-3 my-3 shadow">
                <div class="col-lg-11 col-md-10 col-10">
                    <h3>External en-suites</h3>
                </div>
                <div class="col-lg-1 col-md-2 col-2 qty"><h3>36</h3></div>
            </div>
            <div class="row py-3 my-3 shadow">
                <div class="col-lg-11 col-md-10 col-10">
                    <h3>5th/6th floor en-suites</h3>
                </div>
                <div class="col-lg-1 col-md-2 col-2"><h3>36</h3></div>
            </div>
         </div>
       
     </div>
 </div>

   <!-- room profile end -->


@endsection








@
