<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
        .footer{
            color:white;
            background-color: #333232;
        }
        
        .contacthead {
      border-bottom:1px solid white;
      padding-left:50px;
      font-weight:900;
        }
        .contactinfo{
            padding-left:50px;
        }
        .contactbottom{
      border-bottom:1px solid white;
          
        }
        .facilitithead {
      border-bottom:1px solid white;
      padding-left:50px;
      font-weight:900;
        }
        .facilitiinfo{
            padding-left:50px;
        }
        .facilitibottom{
      border-bottom:1px solid white;
          
        }
        .faciliti{
            margin-top:50px;
        }
        .footerbottom{
            font-weight:100;
        }
        .contactinfo h6{
            font-weight:100;
        }
        .facilitiinfo h6{
            font-weight:100;
        }

    </style>
  </head>
  <body>
      <!-- footer  -->
      <div class="container-fluid footer " style='margin-top:100px'>

          <div class="row contact mx-4 ">
              <div class="row contacthead pt-4"><h2 >Contact</h2></div>
              <div class="row mt-1 contactinfo">
                  <div class="col-lg-3 col-md-6 "><h3>Main Office</h3>
                  <div class="row ml-3"><h6>020 8000 0000</h6></div>
                  <div class="row "><h6>info@studytel.com</h6></div>
                </div>
                  <div class="col-lg-3 col-md-6"><h3>Reservations</h3>
                  <div class="row "><h6>020 8000 0002</h6></div>
                  <div class="row "><h6>reservations@studytel.com</h6></div>
                </div>
                  <div class="col-lg-3 col-md-6"><h3>Administration</h3>
                  <div class="row "><h6>020 8000 0003</h6></div>
                  <div class="row "><h6>admin@studytel.com</h6></div>
                </div>
                  <div class="col-lg-3 col-md-6"><h3>Help</h3>
                  <div class="row "><h6>020 8000 0004</h6></div>
                  <div class="row "><h6>help@studytel.com</h6></div>
                </div>
              </div>
          </div>


          <div class="row faciliti mx-4 ">
              <div class="row facilitithead"><h2 >Facilities</h2></div>
              <div class="row  facilitiinfo pt-3" style='font-weight:200;font-size:20px'>
                  <div class="col-lg-3 col-sm-6"><h6>Business Center</h6>
                  <h6>Metting Rooms</h6>
                </div>
                  <div class="col-lg-3 col-sm-6"><h6>Cinema</h6>
                  <h6>Wellness Lounge</h6>
                </div>
                  <div class="col-lg-3 col-sm-6"><h6>UMBER1 Cafe</h6>
                  <h6>Dinning</h6>
                </div>
                  <div class="col-lg-3 col-sm-6"><h6>Fitness Studio</h6>
                  <h6>Gymnasium</h6>
                </div>
              </div>
             
              
          </div>
          <div class="row mt-5 pt-5 mx-2 pb-3 footerbottom">
              <div class="col-lg-2   text-center">Terms & Conditions</div>
              <div class="col-lg-7 text-center">Studytel Penryn, Kernic Industrial Estate, Penryn, Falmouth Cornwall TR10 9EP</div>
              <div class="col-lg-3 text-center" style='padding-left:40px;'>@ 2020 Copyright Studytel-Penryn</div>
          </div>

      </div>

 <!-- footer end  -->



  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>