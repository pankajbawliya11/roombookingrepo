<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"/>


    <title>Hello, world!</title>
    <style>
        .headeruper{
            border-bottom:0.6px solid lightgrey;
        }
        .mainheader img {
            width:100px;
            height:100px;
           

            border-radius:50%;
        }
        .headertitle h3{
            font-weight:100;

        }
        .headertitle h1{
            color:gold;
            font-weight:600;
            font-size:2rem;
        }
        .headerbtn .btn {
            width:100%;
            color:white;
            font-weight:300;
        }
        @media screen and (max-width: 770px) {
            .mainheader img {
            width:150px;
            height:150px;
            border-radius:50%;

        }
}
      
    </style>
  </head>
  <body class='bg-dark'>
      <!-- header start  -->
 <div class="container-fluid bg-dark " style='color:white'>
     <!-- header uper  -->
     <div class="row py-4 headeruper " >
         <div class="col-lg-2  col-md-2   col-4"><h2>STUDYTEL</h2></div>
         <div class="col-lg-8 col-md-7  col-2"></div>
         <div class="col-lg-1 col-md-2   col-3">
         <a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">    
         <div class="btn btn-outline-light">LOGOUT</div></a>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
        </div>
         <div class="col-lg-1 col-md-1  col-3"><div class="btn btn-light">HELP</div></div>
     </div>
     <!-- header uper close  -->

     <!-- main header  -->
 <div class="row mainheader py-4 px-5 mt-3" style='border-bottom:0.6px solid lightgrey'>
     <div class="col-md-2  col-12 py-2  " style='text-align:center'>
         <img src="images/header-image.jpg" alt="" class='center'>
     </div>
     <div class="col-md-10 col-12 headertitle ">
         <div class="row">
             <div class="col-md-10 col-12"> <h3 class='text-center' >Studeytel Penryn</h3></div>
             <div class="col-2  d-none d-md-block" style='padding-left:65px'><h3 class='text-center' >Hi</h3></div>
        </div>
         <div class="row">
             <div class="col-md-10 col-12"> <h1 class='h1 text-center' >Administration Dashboard</h1></div>
             <div class="col-2 d-none d-md-block"><h1 class='text-center' >John</h1></div>
        </div>
     </div>

     <!-- header button  -->
       
      <div class="row headerbtn my-5" style='margin-left:-9px' >
          <div class="col-md-4  col-12 ">
              <a href="room"><div class="row btn  m-2 py-2" style='background-color:#0aff00'>ROOM BOOKINGS</div></a>
          </div>
          <div class="col-md-4 col-12">
              <a href="profile"><div class="row btn  m-2 py-2" style='background-color:#f86363'> FACILITIES MANAGEMENT</div></a>
          </div>
          <div class="col-md-4 col-12">
              <a href=""><div class="row btn  m-2 py-2" style='background-color:#747170'>CHATS</div></a>
          </div>
      </div>

     <!-- header button end  -->
    


 </div>
     <!-- main header close  -->
 </div>
  <!-- header close  -->

 

 


  
   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>