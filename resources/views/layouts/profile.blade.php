@extends('layouts.main')

@section('css')
<style>

.heading{
    border-bottom:1px solid #0aff00;
    font-weight:100;
    color:white;
}
.heading h1{
    font-weight:100;
    color:white;
}
h5{
    border-bottom:1px solid white;
}
.profile .col{
    background-color: #fff;
    color:black;
    padding-bottom:50px;
    width:100%;
}
.input{
    border-bottom:1px solid grey;
}
.input input{
    border:none;
}
.input input:focus{
    border:none;
}
textarea {
  width: 100%;
  height: 150px;
  padding: 12px 20px;
  /* box-sizing: border-box; */
  border: none;
  /* border-radius: 4px; */
  /* background-color: #f8f8f8; */
  font-size: 16px;
  resize: none;
  
}
i{
    font-size:25px;
}
.icon{
    position:absolute;
    top:0px;
    right:15px;
}


  </style>
@endsection

@section('main-section')
 <!-- main section start  -->

 <div class="container">
     <div class="row heading">
         <div class="col-11"><div class="row   mt-5"><h1>Room Bookings</h1></div></div>
         <div class="col-1 pt-5"><h1><i class="fas fa-arrow-left"></h1></i></div>
     </div>
     <div class="row pb-3  " ><h5 class='pb-4' style='font-weight:100;color:white'>Live chat with booked in students</h5></div>
  

     <!-- main profile page  -->
     <div class="row profile m-sm-0 m-2 ">
         <div class="col" style='position:relative'>
             <form action="">
                 <div class='icon'><i class=" m-3 fas fa-pen-square edit "></i><i class="fas fa-times-circle text-danger"></i></div>
             <div class="row  m-2">
                 <div class="col-md-6 col-12 p-3">
                    <div class="row my-3 input">
                        <label for="fname">First Name</label>
                        <input type="text" placeholder='Sarah'>
                    </div>
                    <div class="row my-3 input">
                        <label for="email">Email</label>
                        <input type="Email" placeholder='sarah@gmail.com'>
                    </div>
                    <div class="row my-3 input">
                        <label for="mobile">Mobile</label>
                        <input type="text" placeholder='0770000000'>
                    </div>
                    <div class="row my-3" >
                        <div class="col-4">
                            <img src="images/header-image.jpg" width='150px' height='200px' alt="">
                        </div>
                        <div class="col-6">
                            Profile Photo
                        </div>
                    </div>
                    <div class="row my-3 input">
                        <label for="Room">Room</label>
                        <input type="text" placeholder='101'>
                    </div>
                    <div class="row my-3 input">
                        <label for="Arrive">Arrive</label>
                        <input type="Email" placeholder='11 sep 2020'>
                    </div>
                   

                 </div>
                 <div class="col-md-6 col-12 p-3">
                 <div class="row my-3 d-md-block d-none input">
                        <label for="lname">Last Name</label>
                        <input type="text" placeholder='Smith'>
                    </div>
                    <div class="row my-3 input">
                        <label for="Gender">Gender</label>
                        <input type="Email" placeholder='Female'>
                    </div>
                    <div class="row my-3 input mt-5">
                        <label for="Address">Address</label>
                        <textarea></textarea>
                    </div>
                    <div class="row my-3 input mt-5">
                        <label for="roomtype">Room Type</label>
                        <input type="text" placeholder='Delux Studio'>
                    </div>
                    <div class="row my-3 input mt-5">
                        <label for="departure">Departure</label>
                        <input type="Email" placeholder='15 sep 2020'>
                    </div>
                 </div>
             </div>
             <div class="row">
                
                 <div class="col-6 p-3">
                
                   
                 </div>
             </div>
             </form>
         </div>

     </div>

     <!-- main profile page end  -->
 </div>

 
    



 <!-- main section start end -->

 


@endsection








@
