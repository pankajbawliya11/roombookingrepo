@extends('layouts.main')

@section('css')
<style>

.heading{
    border-bottom:1px solid #0aff00;
    font-weight:100;
    color:white;
}
.heading h1{
    font-weight:100;
    color:white;
}
h5{
    border-bottom:1px solid white;
}
.roomtype{
    background-color: #fff;
    color:black;
}
.roomtype .qty{
    border-left:1px solid grey;
    text-align:center;
}
.shadow {
  -moz-box-shadow:    3px 3px 5px 6px #ccc;
  -webkit-box-shadow: 3px 3px 5px 6px #ccc;
  box-shadow:         3px 3px 5px 6px #ccc;
}
i{
    font-size:30px;
}




  </style>
@endsection

@section('main-section')
 <!-- main section start  -->

 <div class="container">
     <div class="row heading">
         <div class="col-11"><div class="row   mt-5"><h1>Room Bookings</h1></div></div>
         <div class="col-1 pt-5"><h1><i class="fas fa-arrow-left"></h1></i></div>
     </div>
     <div class="row pb-3  " ><h5 class='pb-4' style='font-weight:100;color:white'>Live chat with booked in students</h5></div>
  

     <!-- main profile page  -->
     <div class="row  mb-5 roomtype px-4" >
         <div class="col pb-5">
            <div class="row">
                <div class="col-lg-9 col-md-7 col-7 ">Room Types</div>
                <div class="col-lg-2 col-md-3 col-3 ">Yes/No Occupied</div>
                <div class="col-lg-1 col-md-2 col-2 ">Health</div>
            </div>
            <div class="row py-2 my-1 shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:lightgreen'><i class="fas fa-check-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:lightgreen'><i class="fas fa-circle"></i></div>
            </div>
            <div class="row py-2 my-1 shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:red'><i class="fas fa-times-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:red'><i class="fas fa-circle"></i></div>
            </div>
            <div class="row py-2 my-1 shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:lightgreen'><i class="fas fa-check-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:lightgreen'><i class="fas fa-circle"></i></div>
            </div>
            <div class="row py-2 my-1 shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:lightgreen'><i class="fas fa-check-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:orange'><i class="fas fa-circle"></i></div>
            </div>
            <div class="row py-2 my-1 shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:lightgreen'><i class="fas fa-check-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:lightgreen'><i class="fas fa-circle"></i></div>
            </div>
            <div class="row py-2 my-1  shadow">
                <div class="col-lg-9 col-md-7 col-8">
                    <h5>Room Number 1</h5>
                </div>
                <div class="col-lg-2 col-md-3 col-2 qty " style='color:red'><i class="fas fa-times-circle"></i></div>
                <div class="col-lg-1 col-md-2 col-2 qty " style='color:lightgreen'><i class="fas fa-circle"></i></div>
            </div>
         </div>
       
     </div>

     <!-- main profile page end  -->
 </div>

 
    



 <!-- main section start end -->

 


@endsection








@
